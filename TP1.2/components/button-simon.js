Vue.component ('button-simon', {
    props : ['data'],
    template : `<div v-bind:class="[data.activeColor ? data.color : '', data.text]"  class="square" v-on:click="selectSquare(data.text)" ></div>`,
    methods : {
        selectSquare(data) {
            this.$emit('click-button', data);
        }
    }
});
