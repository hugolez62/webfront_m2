Vue.component('level-count', {
    props: ['data'],
    template: `
            <h3>Level : {{ data }}</h3>
    `
});
