var app = new Vue({
    el: '#app',
    data: {
        gameRunning: false,
        sequence: [],
        sequenceTmp: [],
        topLeft: { text: 'topLeft', activeColor: false, color: 'blue'},
        topRight: { text: 'topRight', activeColor: false, color: 'red'},
        bottomLeft: { text: 'bottomLeft', activeColor: false, color: 'green'},
        bottomRight: { text: 'bottomRight', activeColor: false, color: 'yellow'},
        squareMapping: [],
        score: 0
    },
    methods: {
        // Function for set all square grey color
        setAllSquareGreyColor: function () {
            this.topLeft.activeColor = false;
            this.topRight.activeColor = false;
            this.bottomLeft.activeColor = false;
            this.bottomRight.activeColor = false;
        },
        // Function for increment score after a good sequence
        levelUp: function () {
            this.score++;
        },
        // Function for add a square to the sequence
        addNewSquareToSequence: function () {
            let random = this.squareMapping[Math.floor(Math.random() * 4)];
            this.sequence.push(random);
            this.sequenceTmp = this.sequence.slice();
        },
        // Function for select square by user and check if is the good square in the sequence
        selectSquare: function (instruction) {
            if (instruction === this.sequenceTmp[0].text) {
                const that = this;
                app[instruction].activeColor = true;
                setTimeout(function () {
                    that.setAllSquareGreyColor();
                    that.sequenceTmp.shift();
                    if (!that.sequenceTmp[0]) {
                        that.levelUp();
                        that.nextTurn();
                    }
                }, 400);
            } else {
                alert('Vous avez perdu');
            }
        },
        // Function for play a sequence
        playSequence: function (instruction) {
            const that = this;
            setTimeout(function () {
                app[instruction].activeColor = true;
                setTimeout(function () {
                    that.setAllSquareGreyColor();
                    that.sequenceTmp.shift();
                    if (that.sequenceTmp[0]) {
                        that.playSequence(that.sequenceTmp[0].text);
                    } else {
                        that.sequenceTmp = that.sequence.slice();
                    }
                }, 400);
            }, 400);
        },
        // Function for run a new turn
        nextTurn: function () {
            const that = this;
            setTimeout(function () {
                that.addNewSquareToSequence();
                that.setAllSquareGreyColor();
                that.playSequence(that.sequenceTmp[0].text);
            }, 400);
        },
        // Function for initialize and run the game
        startGame: function () {
            this.squareMapping = [this.topLeft, this.topRight, this.bottomLeft, this.bottomRight];
            this.gameRunning = true;
            this.sequence = [];
            this.sequenceTmp = [];
            this.score = 0;
            this.nextTurn();
        }
    }
})