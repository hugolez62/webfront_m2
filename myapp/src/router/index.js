import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/my-games',
    name: 'My Games',
    component: () => import(/* webpackChunkName: "about" */ '../views/MyGames.vue')
  },
  {
    path: '/simon-game',
    name: 'Simon Game',
    component: () => import(/* webpackChunkName: "about" */ '../views/SimonGame.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
