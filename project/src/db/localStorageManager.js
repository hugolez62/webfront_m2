export default class LocalStorageManager {

    static generateId(name) {
        return name.replace(/\W/g, '').toLowerCase();
    }

    static getList() {

        if(!localStorage.getItem('list')) {
            localStorage.setItem('list', '{}');
        }

        return JSON.parse(localStorage.getItem('list'));
    }

    static createList(id, name) {
        const lists = LocalStorageManager.getList();
        lists[id] = {name: name, items: []};
        localStorage.setItem('list', JSON.stringify(lists));
    }

    static getAllListItem() {
        const lists = LocalStorageManager.getList();
        const listItems = [];

        for (const id in lists) {
            listItems.push({id, name: lists[id].name, nbItems: lists[id].items.length});
        }

        return listItems;
    }

    static getListElement(id) {
        return LocalStorageManager.getList()[id];
    }

    static addElementToList(id, nameElement) {
        const lists = LocalStorageManager.getList();
        lists[id].items.push({name: nameElement, checked: false})
        localStorage.setItem('list', JSON.stringify(lists));
    }

    static checkElement(id, nameElement) {
        const lists = LocalStorageManager.getList();

        lists[id].items = lists[id].items.map((element) => {
            if(element.name === nameElement) {
                element.checked = !element.checked;
            }
            return element;
        });

        localStorage.setItem('list', JSON.stringify(lists));
    }

    static deleteElement(id, nameElement) {
        const lists = LocalStorageManager.getList();
        lists[id].items = lists[id].items.filter((element) => element.name !== nameElement);
        localStorage.setItem('list', JSON.stringify(lists));
    }

}