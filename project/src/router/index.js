import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import List from '../views/List.vue'
import New from '../views/New.vue'
import Details from '../views/Details.vue'
import Add from '../views/Add.vue'
import StoreLocator from '../views/StoreLocator.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/list',
    name: 'List',
    component: List
  },
  {
    path: '/list/:listId/add',
    name: 'ListAdd',
    component: Add
  },
  {
    path: '/new',
    name: 'New',
    component: New
  },
  {
    path: '/list/:listId',
    name: 'Details',
    component: Details
  },
  {
    path: '/store-locator',
    name: 'StoreLocator',
    component: StoreLocator
  }
]

const router = new VueRouter({
  routes
})

export default router
